import { Injectable } from '@angular/core';
import { MockData } from './mock-data';

@Injectable()
export class MockServiceService {


  constructor() { }

  getData() {
    return MockData;
  }

}
