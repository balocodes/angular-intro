import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-course-one',
  templateUrl: './course-one.component.html',
  styleUrls: ['./course-one.component.css']
})
export class CourseOneComponent implements OnInit {

  my_name;
  error;

  constructor() { }

  ngOnInit() {
  }

  validateForm(){
    if(this.my_name == "Amin") {
      this.error = ""
      console.log("No problem")
    } else {
      this.error = "Invalid name";
    }
  }

  clearForm() {
    this.my_name = ""
    this.error = ""
  }

}
