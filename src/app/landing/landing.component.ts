import { Component, OnInit } from '@angular/core';
import { LastServiceService } from '../last-service.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  name;
  email;
  age;
  users;

  constructor(private lastService: LastServiceService) { }

  ngOnInit() {
  }

  submit() {
    this.lastService.getData("http://localhost:3009/user/allusers").subscribe(
      res => {
        this.users = res;
      }, err => {
        console.log("Error from last service: \n", err);
      }
    );
  }

  postSubmit() {
    const data = {
      name: this.name,
      email: this.email,
      age: this.age
    };
    this.lastService.postData("http://localhost:3009/user/register", data).subscribe(
      res => {
        window.alert(res['message']);
      }, err => {
        console.log(err);
      }
    )
  }

  // sayHi(){
  //   alert("Hi there!")
  // }

  // blink(){
  // }

  // openDialog(){
  //   let the_modal = document.getElementById('myDialog')
  //   the_modal.showModal();
  // }

  // closeDialog() {
  //   let the_modal = document.getElementById('myDialog')
  //   the_modal.close();
  // }


}
