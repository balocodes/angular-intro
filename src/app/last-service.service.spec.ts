import { TestBed, inject } from '@angular/core/testing';

import { LastServiceService } from './last-service.service';

describe('LastServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LastServiceService]
    });
  });

  it('should be created', inject([LastServiceService], (service: LastServiceService) => {
    expect(service).toBeTruthy();
  }));
});
