import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CourseOneComponent } from './course-one/course-one.component';
import { Course2Component } from './course-2/course-2.component';
import { Course3Component } from './course-3/course-3.component';
import { ErrorComponent } from './error/error.component';
import { MockServiceService } from './mock-service.service';
import { UserService } from './user.service';
import { LastServiceService } from './last-service.service';

const ROUTES = [
  {path: '', component: LandingComponent},
  {path: 'course-one', component: CourseOneComponent},
  {path: 'course-two', component: Course2Component},
  {path: 'course-three', component: Course3Component},
  {path: 'home', component: LandingComponent},
  {path: '**', component: ErrorComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    HeaderComponent,
    FooterComponent,
    CourseOneComponent,
    Course2Component,
    Course3Component,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    RouterModule.forRoot(ROUTES),
    FormsModule,
    HttpClientModule
  ],
  providers: [MockServiceService, UserService, LastServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
